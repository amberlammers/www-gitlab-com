# A helper module to handle data in blog posts programmatically.
module BlogHelper
  # Given a gitlab_handle, return the `Gitlab::Homepage::Team::Member` object for a person with the matching handle.
  #
  # @param [string] gitlab_handle - the team member's GitLab handle, not including the `@` symbol. So use `tywilliams`, not `@tywilliams`
  # @return [Gitlab::Homepage::Team::Member] - this class is defined in `lib/team/member.rb` and exposed to us through `lib/homepage.rb`
  def author_data(gitlab_handle:)
    Gitlab::Homepage.team.members.detect { |member| member.gitlab == gitlab_handle }
  end

  # Define reading time
  def reading_time(input)
    words_per_minute = 180
    words = input.split.size
    minutes = (words / words_per_minute).floor
    minutes.positive? ? "#{minutes} min read" : "less than 1 minute"
  end
end
